<?php
/**
 * @file
 * Handler for entityreference_plugin_style.
 */

/**
 * Extends the entity reference plugin style to limit search field selection.
 */
class entityreference_efq_views_plugin_style extends entityreference_plugin_style {

  /**
   * Set the default value to null.
   */
  public function option_definition() {
    $options = parent::option_definition();
    // Changing the field to radios, so the default should not be an array.
    $options['search_fields'] = NULL;

    return $options;
  }

  /**
   * Change the settings form options to be radios.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if (isset($form['search_fields'])) {
      $form['search_fields']['#title'] = t('Search field');
      $form['search_fields']['#type'] = 'radios';
      $form['search_fields']['#description'] = t('Select the field that will be searched when using the autocomplete widget.');
    }
  }

}
