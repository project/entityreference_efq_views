<?php
/**
 * @file
 * Plugin definition file.
 */

$plugin = array(
  'title' => t('Views: Filter by an entity reference efq view'),
  'class' => 'EntityReferenceEFQViews_SelectionHandler_Views',
  'weight' => 0,
);
