<?php
/**
 * @file
 * Views integration for Entity Reference EFQ Views.
 */

/**
 * Implements hook_views_plugins().
 */
function entityreference_efq_views_views_plugins() {
  $plugins = array(
    'display' => array(
      'entityreference_efq_views' => array(
        'title' => t('Entity Reference EFQ Views'),
        'admin' => t('Entity Reference EFQ Views Source'),
        'help' => 'Selects referenceable entities for an entity reference field',
        'handler' => 'entityreference_efq_views_plugin_display',
        'uses hook menu' => FALSE,
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'accept attachments' => FALSE,
        // Custom property, used with views_get_applicable_views() to retrieve
        // all views with a 'Entity Reference' display.
        'entityreference display' => TRUE,
      ),
    ),
    'style' => array(
      'entityreference_efq_views_style' => array(
        'title' => t('Entity Reference EFQ Views list'),
        'help' => 'Returns results as a PHP array of labels and rendered rows.',
        'handler' => 'entityreference_efq_views_plugin_style',
        'theme' => 'views_view_unformatted',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'entityreference',
        'even empty' => TRUE,
      ),
    ),
  );
  return $plugins;
}
