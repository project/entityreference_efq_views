<?php
/**
 * @file
 * Handler for entityreference_efq_views_plugin_display.
 */

/**
 * The Entity Reference EFQ Views display plugin extends the existing display.
 */
class entityreference_efq_views_plugin_display extends entityreference_plugin_display {

  /**
   * Update the query method to work with EFQs.
   */
  public function query() {
    $options = $this->get_option('entityreference_options');

    // Play nice with Views UI 'preview' : if the view is not executed through
    // EntityReference_SelectionHandler_Views::getReferencableEntities(),
    // don't alter the query.
    if (empty($options)) {
      return;
    }

    // Make sure the id field is included in the results, and save its alias
    // so that references_plugin_style can retrieve it.
    $this->id_field_alias = $id_field = $this->view->query->add_field($this->view->base_table, $this->view->base_field);
    if (strpos($id_field, '.') === FALSE) {
      $id_field = $this->view->base_table . '.' . $this->id_field_alias;
    }

    // Restrict the autocomplete options based on what's been typed already.
    if (isset($options['match'])) {
      $efquery = $this->view->query->query;
      $style_options = $this->get_option('style_options');
      $search_field = $style_options['search_fields'];

      if (!empty($search_field)) {
        // Determine if we need a property or field condition.
        $field = field_info_field($search_field);
        if (!empty($field)) {
          // @TODO: not all fields use the value suffix.
          $efquery->fieldCondition($search_field, 'value', $options['match'], $options['match_operator']);
        }
        else {
          $efquery->propertyCondition($search_field, $options['match'], $options['match_operator']);
        }
      }
    }

    // Add an IN condition for validation.
    if (!empty($options['ids'])) {
      $this->view->query->add_where(NULL, $id_field, $options['ids']);
    }

    $this->view->set_items_per_page($options['limit']);
  }

}
